package com.codementor.authentication

import com.codementor.authentication.token.TokenService
import com.codementor.response.HeaderService
import io.jsonwebtoken.impl.crypto.MacProvider
import org.springframework.http.MediaType
import org.springframework.http.RequestEntity
import spock.lang.Specification
import spock.lang.Unroll

import javax.crypto.SecretKey

class UsersSpec extends Specification {

    UserRepository repository = Mock(UserRepository)
    HeaderService headerService = new HeaderService()
    SecretKey key = MacProvider.generateKey()
    TokenService tokenService = new TokenService(key,repository)
    Users users = new Users(tokenService, repository, new PasswordValidationService(), headerService)

    @Unroll
    def "should return bad request for malformed email:"() {
        given:


            def requestEntity = RequestEntity.post(new URI("http://localhost")).contentType(MediaType.APPLICATION_JSON).body(UserDto.builder().email(email).build())
        when:
            def responseEntity = users.signUp(requestEntity);
        then:
            responseEntity.statusCode.value() == 400
        where:
            email  || _
            "1 "   || _
            "@1 "  || _
            "1@b"  || _
            "1 @b" || _
    }

    @Unroll
    def "should return bad request for not valid password"() {
        given:


            def requestEntity = RequestEntity.post(new URI("http://localhost"))
                                             .contentType(MediaType.APPLICATION_JSON)
                                             .body(UserDto.builder().email("jon@doe.com").password(password).build())
        when:
            def responseEntity = users.signUp(requestEntity);
        then:
            responseEntity.statusCode.value() == 400
            responseEntity.body.validation == validationMessage
        where:
            password      || validationMessage
            "1 av # D AA" || '[ILLEGAL_WHITESPACE]'
            "abcd"        || '[TOO_SHORT, INSUFFICIENT_UPPERCASE, INSUFFICIENT_DIGIT, INSUFFICIENT_SPECIAL, ILLEGAL_SEQUENCE, ILLEGAL_SEQUENCE]'
            "11234"       || '[TOO_SHORT, INSUFFICIENT_UPPERCASE, INSUFFICIENT_SPECIAL, ILLEGAL_SEQUENCE, ILLEGAL_SEQUENCE, ILLEGAL_SEQUENCE, ILLEGAL_SEQUENCE]'
            "QWEret22"    || '[INSUFFICIENT_SPECIAL, ILLEGAL_SEQUENCE, ILLEGAL_SEQUENCE]'
    }

    def "should return 409 when email already exists in database"() {

        given:
            def email = "jon@doe.com"

            def requestEntity = RequestEntity.post(new URI("http://localhost"))
                                             .contentType(MediaType.APPLICATION_JSON)
                                             .body(UserDto.builder().email(email).password("12QarT%a").build())
            repository.exists(email) >> true;
        when:
            def responseEntity = users.signUp(requestEntity);
        then:
            responseEntity.statusCode.value() == 409
            responseEntity.body.validation == "Email Already Taken"
    }

    def "should create user in database and return filled token when all required fields are set"() {

        given:
            def email = "jon@doe.com"

            def requestEntity = RequestEntity.post(new URI("http://localhost"))
                                             .contentType(MediaType.APPLICATION_JSON)
                                             .body(UserDto.builder().email(email).password("12QarT%a").username("username").build())
            repository.exists(email) >> false;

        when:
            def responseEntity = users.signUp(requestEntity);
        then:
            1 * repository.saveAndFlush(_)
            responseEntity.statusCode.value() == 200
            !responseEntity.body.jwt.isEmpty()
            !responseEntity.body.refreshToken.isEmpty()
            responseEntity.body.validation == null
    }
}
