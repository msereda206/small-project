package com.codementor.authentication

import com.codementor.authentication.token.TokenService
import com.codementor.response.HeaderService
import io.jsonwebtoken.impl.crypto.MacProvider
import org.springframework.http.RequestEntity
import spock.lang.Specification

class AuthenticationSpec extends Specification {


    UserRepository repository = Mock(UserRepository)
    TokenService tokenService = new TokenService(MacProvider.generateKey(),repository)
    Authentication authentication = new Authentication(tokenService, new HeaderService(), repository)

    def "should be able to log in an known user with correct credentials"() {
        given:

            def email = "a@b.c"
            def pass = "pass"
            UserDto user = UserDto.builder().email(email).password(pass).build()
            def requestEntity = RequestEntity.post(URI.create("http://localhost")).body(user)
            def build = UserDto.builder().email(email).password(pass).build()
            repository.findOne(email) >> build
        when:
            def login = authentication.login(requestEntity);
            println login
        then:
            login.statusCodeValue == 200
            login.body.refreshToken != null;
            login.body.jwt != null;
    }

    def "should Return status 401 for missing email"() {

        given:
            def email = ""
            def pass = "pass"
            UserDto user = UserDto.builder().email(email).password(pass).build()
            def requestEntity = RequestEntity.post(URI.create("http://localhost")).body(user)
        when:
            def login = authentication.login(requestEntity);
        then:
            login.statusCodeValue == 401
            login.body.refreshToken == null;
            login.body.jwt == null;
    }

    def "should Return status 401 for missing password"() {

        given:
            def email = "aaa@bbb.ccc"
            def pass = ""
            UserDto user = UserDto.builder().email(email).password(pass).build()
            def requestEntity = RequestEntity.post(URI.create("http://localhost")).body(user)
        when:
            def login = authentication.login(requestEntity);
        then:
            login.statusCodeValue == 401
            login.body.refreshToken == null;
            login.body.jwt == null;
    }

    def "should return 401 for not matching credentials"() {
        def email = "a@b.c"
        def pass = "pass"
        given:
            UserDto user = UserDto.builder().email(email).password(pass).build()
            def requestEntity = RequestEntity.post(URI.create("http://localhost")).body(user)
            def build = UserDto.builder().email(email).password("!Q3a4Dac!").build()
            repository.findOne(email) >> build
        when:
            def login = authentication.login(requestEntity);
        then:
            login.statusCodeValue == 401
            login.body.refreshToken == null;
            login.body.jwt == null;
    }

    def "should return 401 on refresh token for not logged user"() {
        def email = "a@b.c"
        def pass = "pass"
        given:

            UserDto user = UserDto.builder().email(email).password(pass).build()
            def token = tokenService.buildToken(user)
            user.setRefreshToken(token.refreshToken);
            def requestEntity = RequestEntity.post(URI.create("http://localhost")).body(user)
            def build = UserDto.builder().email(email).password("!Q3a4Dac!").build()
            repository.findOne(email) >> build
        when:
            def login = authentication.refreshToken(requestEntity);
        then:
            login.statusCodeValue == 401
            login.body.refreshToken == null;
            login.body.jwt == null;
    }
    def "should return 200 on refresh token for  logged user"() {
        def email = "a@b.c"
        def pass = "pass"
        given:

            UserDto user = UserDto.builder().email(email).password(pass).build()
            def token = tokenService.buildToken(user)
            user.setRefreshToken(token.refreshToken);
            def requestEntity = RequestEntity.post(URI.create("http://localhost")).body(user)
            def build = UserDto.builder().email(email).password("!Q3a4Dac!").refreshToken(token.refreshToken).build()
            repository.findOne(email) >> build
        when:
            def login = authentication.refreshToken(requestEntity);
        then:
            login.statusCodeValue == 200
            login.body.refreshToken == null;
            login.body.jwt != null;
    }

    def "should return 200 on log out"() {
        def email = "a@b.c"
        def pass = "pass"
        given:

            UserDto user = UserDto.builder().email(email).password(pass).build()
            def token = tokenService.buildToken(user)
            user.setRefreshToken(token.refreshToken);
            def requestEntity = RequestEntity.post(URI.create("http://localhost")).body(user)
            def build = UserDto.builder().email(email).password("!Q3a4Dac!").refreshToken(token.refreshToken).build()
            repository.findOne(email) >> build
        when:
            def login = authentication.logout(requestEntity);
        then:
            login.statusCodeValue == 200
            1 * repository.saveAndFlush(_)
    }
}
