package com.codementor.authentication.token

import com.codementor.authentication.UserDto
import com.codementor.authentication.UserRepository
import io.jsonwebtoken.impl.crypto.MacProvider
import spock.lang.Specification

import javax.crypto.SecretKey

class TokenServicespec extends Specification {
    SecretKey key = MacProvider.generateKey();
    UserRepository repository = Mock(UserRepository)
    TokenService tokenService = new TokenService(key, repository)

    def "should mark self created token as valid for logged user"() {
        def email = "a.b@c.d"
        def token1 = tokenService.buildToken(UserDto.builder().email(email).build())
        given:
            def token = Optional.of(token1.jwt);
            repository.findOne(email) >> UserDto.builder().refreshToken(token1.refreshToken).build()
        expect:
            tokenService.isTokenValid(token)

    }

    def "should mark self created token as invalid for logged out user"() {
        def email = "a.b@c.d"
        def token1 = tokenService.buildToken(UserDto.builder().email(email).build())
        given:
            def token = Optional.of(token1.jwt);
            repository.findOne(email) >> UserDto.builder().build()
        expect:
            !tokenService.isTokenValid(token)

    }

    def "should mark no created token as invalid "() {
        given:
            def token = Optional.empty();
        expect:
            !tokenService.isTokenValid(token)
    }

    def "should mark tamperedString token as invalid "() {
        given:
            def token = Optional.of("1231231");
        expect:
            !tokenService.isTokenValid(token)
    }

    def "should mark refresh token as invalid Access token"() {
        def email = "a.b@c.d"
        def token1 = tokenService.buildToken(UserDto.builder().email(email).build())
        given:
            def token = Optional.of(token1.refreshToken);
            repository.findOne(email) >> UserDto.builder().refreshToken(token1.refreshToken).build()
        expect:
            !tokenService.isTokenValid(token)

    }

    def "should mark refresh token created with other key as invalid Access token"() {
        def email = "a.b@c.d"
        TokenService tokenService2 = new TokenService(MacProvider.generateKey(), repository)
        def token1 = tokenService2.buildToken(UserDto.builder().email(email).build())
        given:
            def token = Optional.of(token1.refreshToken);
            repository.findOne(email) >> UserDto.builder().refreshToken(token1.refreshToken).build()
        expect:
            !tokenService.isTokenValid(token)

    }

}
