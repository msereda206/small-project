package com.codementor.ideas;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.codementor.authentication.UserDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
class IdeaDto {
    private String content;
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid.hex")
    private String id;
    @NotNull
    private Integer impact;
    @NotNull
    private Integer ease;
    @NotNull
    private Integer confidence;
    @NotNull
    @JsonProperty("average_score")
    private Double averangeScore;
    @NotNull
    @JsonProperty("created_at")
    private Long createdAt;

    @ManyToOne
    @JoinColumn(name="email")
    @JsonIgnore
    private UserDto user;

    @Transient
    String validation;
}
